# KKDK 2021 nr 1

![kkdataklubben](kkdk.gif)

---

> A man using Apple maps walks into a bar.
> Or a pharmacy, or maybe a shoe store.

---

![Zooms](https://media2.giphy.com/media/orVa44Oav5WoF1LVOE/giphy.gif?cid=ecf05e47csqq5hlifftwfpvxr5hi7ng7v1vpthdhaij6hi8d&rid=giphy.gif&ct=g)

---

[Windy.com](https://windy.com)

--- 

We're going to make a web map by populating this repository. It should end up looking like this, with the top folder containing two things only - the "public" folder, and the file ".gitlab-ci.yml". 

--- 

Like so:

- ./
- .gitlab-ci.yml
- public/
  - index.html
  - style.css
  - map.js
  - data.json

Each file will be explained underneath.

---

## 1 .gitlab-ci.yml
Notice the leading period. This file sets Continious Integration (CI) configuration for a Gitlab pipeline, meaning we're setting up our code to be hosted on Gitlab Pages. In here we'll tell Gitlab.io to publish our "public" folder to a public URL.

---
<!-- .slide: data-background-video="Stream.mp4" -->

- Git, Github and GitLab 
- CI

---

#### Task 1.1: Create a Gitlab.com-user.
#### Task 1.2: Create a blank Gitlab project and the CI file .gitlab-ci.yml
Hint 1: Fire up the Gitlab Web IDE

Hint 2: The file should look like this. 

```
# .gitlab-ci.yml
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/plain-html
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```


## 2 public
Is just a folder. In step 1 we configured Gitlab to web host files from here. Git is a files based system, so everything is either a file or a folder.

---

#### Task 2: Create the "public" folder in your Gitlab project.
Hint: You can do it by creating a file called public/index.html, which will automatically create the folder.

--- 

## 3 public/index.html

Will load in styles and javascript libraries in the head: style.css and leaflet.js,  with a body consisting of a ``<div>`` with our mapid, and one ``<script>`` tag to load our map.js.

---

- Who has done HTML before?
- How do you open a .html?
- What is a tag?

---

Example: Head and body.

```
<html>
  <head>
    <title>Examples for days</title>
    <script src="example.js" />
    <link rel="stylesheet" href="example.css" />

  </head>
  <body>
    <h1>Hello world</h1>
    Crazy times.
  </body>
</html>
```

---

#### Task 3.1: Make a "Hello World web page", and find the URL.

Hint 1: Tasks in step 1 above need to be completed before this works.

Hint 2: Use the code above to suit your needs.

---

#### Task 3.2: Make the HTML file ready to display a map

Hint: Copy the code below into your index.html.

---

```
<html>
	<head>
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
		 <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	</head>
	</body>
		<div id="mapid"></div>

		<script type="text/javascript" src="map.js"></script>
	</body>

</html>
```

---


## /map.js
Is our Javascript to configure leaflet.js with our data. 

---

## data.json
JSON is short for Javascript Object Notation, and is a way to format and store data. We will insert interesting data here.
